# Highsys Export

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/highsys/highsys_notebooks/master?filepath=home%2Fjovyan%2Fnotebooks%2FREADME.ipynb)

# Fonctionnement

L'outil est divisé en deux parties :
* Une partie collecte des données
* Une partie analyse des données

## Collecte des données

La collecte des données s'effectue via l'utilisation de l'API SOAP Boond Manager.

Cette collecte est un mélange entre PHP (pour garder la compatibilité avec l'API SOAP et les fonctions natives PHP) et Python (pour le parse des données et l'utilisation des DataFrames).

La collecte des données appel un script PHP présent dans php_function, query_tab.php.

Celui-ci prend en paramètre la requête SQL a traité, requêtre présente dans le fichier config.ini

Le resultat (sous la forme d'un bloc HTML comprenant une balise <html>) est enregistré dans un fichier TXT. Celui-ci sera par la suite ouverture en lecture, son contenu parser, puis ajouter a un DataFrame).

Ce même DataFrame sera sauvegardé sous le format MSGPACK dans le répertoire dump_data

Exemple d'un appel d'un script PHP (bas niveau):
```python
subprocess.call([
    "php",
    "../php_function/query_tab.php",
    "../config.ini",
    "../php_function/query_TAB_PROFIL.txt",
    "TAB_PROFIL
])
```

Exemple d'un appel (haut niveau) pour la récupération de la table TAB_PROFIL :
```python
tools.export_table_boon("TAB_PROFIL")
```

Le notebook boon_export_table permet d'effectuer la récupération des tables de Boon.

## Analyse des données

L'analyse des données s'effectue via Jupyter Notebook

L'accès s'effectue via Binder

## Developpement

Pour développer l'application, voici les opérations :
* python3 -m venv env
* source env/bin/activate
* pip install -r requierements.txt
* PYTHONPATH=$PWD/notebooks jupyter notebook --config=notebooks/jupyter_notebook_config.py --ip=0.0.0.0 --port=8888

Il est vivement conseillé d'utiliser l'image Docker (même fonctionnement que Binder)

## Build des images

docker build -t registry.gitlab.com/highsys/highsys_notebooks/notebook:latest .
docker push registry.gitlab.com/highsys/highsys_notebooks/notebook:latest

## Utilisation des images

docker run registry.gitlab.com/highsys/highsys_notebooks/notebook:latest jupyter notebook --ip=0.0.0.0 --port=8888