import pandas
import os
import datetime

def get_anniversaire(jour=datetime.datetime.strftime(datetime.datetime.today(), "%d"), mois=datetime.datetime.strftime(datetime.datetime.today(), "%m")):
    """
        Renvoie un DataFrame contenant les personnes ayant le mois jour en date de naissance
    """
    df = pandas.read_msgpack(os.environ["PYTHONPATH"] + "/../dump_data/TAB_PROFIL.msgpack")
    return df[
        (df["PROFIL_DATENAISSANCE"].str.contains(f"{mois}-{jour}")) & 
        (df["PROFIL_TYPE"] == 0) &
        (df["PROFIL_ETAT"] == 1)
    ][["PROFIL_NOM", "PROFIL_PRENOM", "PROFIL_DATENAISSANCE", "PROFIL_EMAIL"]]

def get_effectifs():
    """
        Renvoie un DataFrame contenant les différents type de profil et le nombre associés
    """
    df = pandas.read_msgpack(os.environ["PYTHONPATH"] + "/../dump_data/TAB_PROFIL.msgpack")
    df_profil = df[df["PROFIL_ETAT"] == 1].groupby(["PROFIL_TYPE"]).agg(["count"])["PROFIL_NOM"].reset_index()
    df_profil_value = pandas.read_msgpack(os.environ["PYTHONPATH"] + "/../dump_data/PROFIL_VALUE.msgpack")

    #On merge le resultat
    df_merge = pandas.merge(df_profil, df_profil_value, on='PROFIL_TYPE')[["PROFIL_TYPE_NAME", "count"]]
    return df_merge

def get_contrats(mois=datetime.datetime.strftime(datetime.datetime.today(), "%m"), annee=datetime.datetime.strftime(datetime.datetime.today(), "%Y")):
    """
        Renvoie un DataFrame contenant les contrats encore en cours (CTR_FIN >= DATE)
    """
    df = pandas.read_msgpack(os.environ["PYTHONPATH"] + "/../dump_data/TAB_CONTRAT.msgpack")
    return df[(df["CTR_FIN"] >= f"{annee}-{mois}")]

def get_new_profils(mois=datetime.datetime.strftime(datetime.datetime.today(), "%m"), annee=datetime.datetime.strftime(datetime.datetime.today(), "%Y")):
    """
        Renvoie un DataFrame contenant les personnes ayant eu leur profil de créer pour ce mois et cette année
    """
    df = pandas.read_msgpack(os.environ["PYTHONPATH"] + "/../dump_data/TAB_PROFIL.msgpack")
    df["PROFIL_DATE"] = pandas.to_datetime(df["PROFIL_DATE"])
    return df[(df["PROFIL_DATE"].dt.year == int(annee)) & (df["PROFIL_DATE"].dt.month==int(mois))]

def get_factures(mois=datetime.datetime.strftime(datetime.datetime.today(), "%m"), annee=datetime.datetime.strftime(datetime.datetime.today(), "%Y")):
    """
        Renvoie un Dataframe contenant les factures ainsi que les lignes de facturation pour le mois et l'année
    """
    df_factures = pandas.read_msgpack(os.environ["PYTHONPATH"] + "/../dump_data/TAB_FACTURATION.msgpack")
    df_itemfacture = pandas.read_msgpack(os.environ["PYTHONPATH"] + "/../dump_data/TAB_ITEMFACTURE.msgpack")

    #On merge le resultat
    df_merge = pandas.merge(df_factures, df_itemfacture, on="ID_FACTURATION")
    df_merge["FACT_DATE"] = pandas.to_datetime(df_merge["FACT_DATE"], dayfirst=True)
    df_merge["FACT_DATEREGLEMENTATTENDUE"] = pandas.to_datetime(df_merge["FACT_DATEREGLEMENTATTENDUE"], dayfirst=True)
    return df_merge[(df_merge["FACT_DATE"].dt.year == int(annee)) & (df_merge["FACT_DATE"].dt.month==int(mois))]