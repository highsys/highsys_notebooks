import ipywidgets as widgets
from IPython.display import display, FileLink

import functions
import tools
import configparser
import os

def export_tables():
    """
        Exporte toutes les tables présentes dans le fichier de configuration
    """
    for key in config["REQUETE"]:
        tools.export_table_boon(key.upper())

def load_config():
    """
        Charge le fichier de configuration
        Vérifie que celui-ci existe, génère une erreur si celui-ci n'est pas présent
    """
    if os.path.isfile(os.environ["PYTHONPATH"] + "/config.ini") == False:
        raise ValueError('Fichier de configuration config.ini non trouvée !. Merci de mettre le fichier config.ini dans le répertoire notebooks.')
    else:
        config = configparser.RawConfigParser()
        config.read(os.environ["PYTHONPATH"] + "/config.ini")
        return config

def export_dataframe(dataframe, export_name="export.xlsx"):
    """
        Exporte le dataframe au format xlsx sans l'index
        Renvoie un filelink du fichier
    """
    dataframe.to_excel(export_name, index=False)
    return FileLink(export_name)


config = load_config()