import subprocess
import os
import logging

import pandas as pd
from IPython.display import FileLink
from babel.dates import format_date, format_datetime, format_time
from bs4 import BeautifulSoup

import functions

logging.basicConfig(level=logging.DEBUG)

def generate_sage_import_from_boon(path_to_boon_export):
    """
        Genere un fichier permettant de faire l'intégration dans Sage
        Ce fichier est issue d'un export Boon
        Args:
            path_to_boon_export (str): Chemin vers le fichier d'export Boon
        Returns:
            str: Chemin vers le fichier généré pour l'import dans SAGE
    """
    output_file = path_to_boon_export.replace("xlsx", "txt").replace("xls", "txt")
    
    #On charge notre fichier
    df = pd.read_excel(path_to_boon_export,converters={'Compte':str})
    df["Date"] = pd.to_datetime(df["Date"], dayfirst=True)

    #On renomme les colonnes
    df.columns = [
        "date",
        "journal",
        "compte_g",
        "piece",
        "libelle",
        "debit",
        "credit",
        "devise"
    ]
    
    #On supprime la colonne devise
    del df["devise"]
    
    # On ajoute une colonne compte_t a droite de compte_g avec les données de compte_g
    df.insert(3, "compte_t", df["compte_g"])
    
    #On ajoute une colonne periodicite a droite de piece
    df.insert(5, "periodicite", "")
    
    #Not now
    """
    #On ajoute une colonne quantite apres la colonne libelle
    df.insert(7, "quantite", "")
    """
    
    #Not now
    """
    #On ajoute une colonne date_echeance apres la colonne quantite
    df.insert(8, "date_echeance", "")
    """
    
    # On supprime les lignes lorsque le nom de piece commence par FACT
    df = df[~df["piece"].str.startswith(("FACT"))]

    # On supprime les valeurs 4457XXX, 706XXX, et 707XXX de la colonne compte_t
    df.loc[df["compte_t"].str.startswith(('706', '4457', '707', '708')), "compte_t"] = ""

    # On ajoute la périodicité (c'est le mois et l'année de la colonne date)
    # Le format doit etre mois-année[2]
    # Exemple avril-19
    #df["periodicite"] = df["date"].dt.strftime("%m-%y")
    def formatdate(x):
        return format_date(x, "MMM-YY", locale='fr_FR').replace(".", "")
    def formatdateecheance(x):
        return format_date(x, "dMMYY", locale='fr_FR').replace(".", "")
    df["periodicite"] = df["date"].apply(formatdate)

    #On doit modifier le format de la colonne date (JJ/MM/YYYY)
    df["date"] = df["date"].dt.strftime('%d/%m/%Y')

    # Dans la colonne compte_g, on met 41100000 sur toutes les lignes commençant par 411
    df.loc[df["compte_g"].str.startswith(("411")), "compte_g"] = "41100000"
    df.loc[df["compte_g"].str.startswith(("412")), "compte_g"] = "41200000" #issue #32



    # On doit ajouter la fin de la colonne compte_t au libellé
    # On ajoute également la fin de la colonne compte_t au libellé des 2 lignes au dessus
    # Exemple
    """
     |date      |journal|compte_g|compte_t  |periodicite|piece         |libelle                 |debit |credit|
    0|2019-01-31|VTE    |70600000|          |2019-01    |F201901HS00029|FACT6786 Normale HT     |1360.0|0.0   |
    1[2019-01-31|VTE    |44571100|          |2019-01    |F201901HS00029|FACT6786 Normale TVA 20%|272.0 |0.0   |
    2|2019-01-31|VTE    |41100000|411NATIXIS|2019-01    |F201901HS00029|FACT6786 Normale TTC    |0.0   |1632.0|

    Doit devenir

     |date      |journal|compte_g|compte_t  |periodicite|piece         |libelle                         |debit |credit|
    0|2019-01-31|VTE    |70600000|          |2019-01    |F201901HS00029|NATIXIS-FACT6786 Normale HT     |1360.0|0.0   |
    1[2019-01-31|VTE    |44571100|          |2019-01    |F201901HS00029|NATIXIS-FACT6786 Normale TVA 20%|272.0 |0.0   |
    2|2019-01-31|VTE    |41100000|411NATIXIS|2019-01    |F201901HS00029|NATIXIS-FACT6786 Normale TTC    |0.0   |1632.0|

    """
    #On boucle sur toutes les valeurs
    for index, row in df.iterrows():
        #Si la colonne compte_t de la ligne courante commence par 411
        if row["compte_t"].startswith("411"):
            #On prend le nom du compte sans les 3 premiers caractères
            short_compte_t = row["compte_t"][3:]
            #On modifie le -2
            df.at[index-2, 'libelle'] = "%s-%s" %(short_compte_t, df.at[index-2, 'libelle'])
            #On modifie le -1
            df.at[index-1, 'libelle'] = "%s-%s" %(short_compte_t, df.at[index-1, 'libelle'])
            #On modifie le 0
            df.at[index, 'libelle'] = "%s-%s" %(short_compte_t, df.at[index, 'libelle'])
        

    #Il faut remplacer le separateur de décimale . par ,
    def formatnumbers(x):
        x = str(x).replace(".", ",")
        return x
    df["debit"] = df["debit"].apply(formatnumbers)
    df["credit"] = df["credit"].apply(formatnumbers)

    #df.to_csv(output_file, header=False, index=False, encoding="CP1252")
    #df.to_csv(output_file, header=False, index=False, sep="\t", encoding="CP1252", line_terminator="\r\n")

    #We need to remove last line
    data = df.to_csv(None, header=False, index=False, sep="\t", encoding="CP1252", line_terminator="\r\n")
    open(output_file, 'w', encoding="CP1252").write(data[:-1])

    return FileLink(output_file)


def export_table_boon(table_name):
    """
        Exporte une table issue de BOON
    """
    import os

    logging.info(f"Call PHP script for {table_name} ...")
    # On execute un script PHP
    res_call = subprocess.call([
        "php",
        os.environ["PYTHONPATH"] + "/../php_function/query_tab.php",
        os.environ["PYTHONPATH"] + "/config.ini",
        os.environ["PYTHONPATH"] + "/../php_function/query_%s.txt" %table_name,
        table_name
    ])

    if res_call != 0:
        logging.info(f"Error on call PHP script !")
        raise ValueError(f"Error on call PHP script !")

    logging.info(f"Read result file ...")
    # On charge notre ficher txt 
    f = open(os.environ["PYTHONPATH"] + "/../php_function/query_%s.txt" %table_name, encoding="utf8")
    data = f.read()
    f.close()

    logging.info(f"Parse HTML result file ...")
    # On parse le HTML
    soup = BeautifulSoup(data,features="lxml")
    # On ne recupere que les balises "table"
    data_soup = soup.select('table')

    logging.info(f"Convert table result to Pandas DataFrame ...")
    # On genere des dataframes a partir des données HTML
    dfs = []
    for i in data_soup:
        dfs.append(pd.read_html(str(i))[0])
    
    logging.info(f"Concat Pandas DataFrame ...")
    # On concatene les différents dataframes
    df = pd.concat(dfs)
    
    logging.info(f"Dump Pandas DataFrame to msgpack ...")
    # On dump le dataframe au format msgpack
    df.to_msgpack(os.environ["PYTHONPATH"] + "/../dump_data/%s.msgpack" %table_name)

    logging.info(f"Remove old file ...")
    # On supprime le fichier texte
    os.remove(os.environ["PYTHONPATH"] + "/../php_function/query_%s.txt" %table_name)

    return os.environ["PYTHONPATH"] + "/../dump_data/%s.msgpack" %table_name