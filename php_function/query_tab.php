<?php

    /* Get destination output */
    $config_path = $argv[1];
    $result_path = $argv[2];
    $tab = $argv[3];

    /* Read config file */
    $ini_array = parse_ini_file($config_path, true );


    defined('soap_domain') || define('soap_domain', $ini_array['SOAP_BOON']['DOMAIN']);
    defined('soap_hash') || define('soap_hash', $ini_array['SOAP_BOON']['HASH']);
    defined('soap_token') || define('soap_token', $ini_array['SOAP_BOON']['TOKEN']);
    defined('result_path') || define('result_path', $result_path);
    defined('soap_tab') || define('soap_tab', $tab);
    defined('soap_query') || define('soap_query', $ini_array['REQUETE'][$tab]); 


    function element_to_obj($element) {
        $obj = array( "tag" => $element->tagName );
        foreach ($element->attributes as $attribute) {
            $obj[$attribute->name] = $attribute->value;
        }
        foreach ($element->childNodes as $subElement) {
            if ($subElement->nodeType == XML_TEXT_NODE) {
                $obj["html"] = $subElement->wholeText;
            }
            else {
                $obj["children"][] = element_to_obj($subElement);
            }
        }
        return $obj;
    }

    function setSignature($token, $hash) {
        $uniqid = uniqid();
        $date = date("YmdHis", mktime());
        $payload = json_encode(array('token' => $token,
                                    'transaction' => array('id' => $uniqid, 'date' => $date)));
        return '<transaction>
                    <id>'.$uniqid.'</id>
                    <date>'.$date.'</date>
                </transaction>
                <token>'.$token.'</token>
                <signature>'.hash_hmac('sha256', $payload, $hash).'</signature>';
    }

    function ReadBDD($requete) {
        return callBoondManager("PublicService",
                                "readbdd", 
                                "<requete>".escapeXMLMessage($requete)."</requete>");
    }

    function callBoondManager($interface, $fonction, $param, $file = null) {
        $soapClient = new SoapClient(soap_domain."/soapi/BoondManager.wsdl", array("trace" => 0));
        $flux = '<webservice>
                    <fonction>'.escapeXMLMessage($fonction).'</fonction>
                    <param>'.setSignature(soap_token, soap_hash).$param.'</param>
                </webservice>';
        if($interface == 'BinaryService')
            return $soapClient->BinaryService($flux);
        else
            return $soapClient->PublicService($flux, $file);
    }

    function escapeXMLMessage($message) {
        return str_replace(array('&',''), array('&amp;','&lt;','&gt;'), $message);
    }

    function format_data($data) {
        $data  = str_replace("<tableau>", "<table>", $data);
        $data  = str_replace("</tableau>", "</table>", $data);
        $data  = str_replace("<entete>", "<th>", $data);
        $data  = str_replace("</entete>", "</th>", $data);
        $data  = str_replace("<ligne>", "<tr>", $data);
        $data  = str_replace("</ligne>", "</tr>", $data);
        $data  = str_replace("<value>", "<td>", $data);
        $data  = str_replace("</value>", "</td>", $data);
        $data  = str_replace("<table><th>", "<table><tr><th>", $data);
        $data  = str_replace("</th><tr>", "</th></tr><tr>", $data);
        return $data;
    }

    function loop_over() {
        //On doit recupere le nombre de ligne de la table
        $soap_tab = soap_tab;
        $soap_query = soap_query;

        $res_count_query = ReadBDD("SELECT COUNT(*) FROM $soap_tab LIMIT 0,1;");    
        preg_match_all('#<value>([^\s]+)</value>#', $res_count_query, $matches);
        $res_count = implode(' ', $matches[1]);

        for ($i = 0; $i <= $res_count; $i+=300) {
            $data = ReadBDD(sprintf($soap_query, $i));
            $data = format_data($data);

            file_put_contents(result_path, $data, FILE_APPEND);
        }

    }

    loop_over();
    
?>